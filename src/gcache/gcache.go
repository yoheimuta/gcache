package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/codegangsta/martini"
	"github.com/garyburd/redigo/redis"
	"github.com/golang/groupcache"
)

func main() {
	addr := os.Getenv("GROUPCACHE_ADDR")
	peers := groupcache.NewHTTPPool("http://" + addr)
	peers.Set("http://127.0.0.1:8000", "http://127.0.0.1:8001")
	go http.ListenAndServe(addr, peers)
	heavy := groupcache.NewGroup("redis", 64<<20, groupcache.GetterFunc(query))

	c := newCache()

	m := martini.Classic()
	m.Get("/_stats", func() []byte {
		v, err := json.Marshal(&heavy.Stats)
		if err != nil {
			log.Print(err)
		}
		return v
	})
	m.Get("/:key", func(params martini.Params) string {
		var result string
		if err := heavy.Get(c, params["key"], groupcache.StringSink(&result)); err != nil {
			log.Print(err)
		}
		return result
	})
	m.Run()
}

type cache struct {
	mu   sync.Mutex
	conn redis.Conn
}

func newCache() *cache {
	timeout := 10 * time.Second
	server := "localhost:6379"

	if conn, err := redis.DialTimeout(
		"tcp",
		server,
		timeout,
		timeout,
		timeout,
	); err != nil {
		panic(fmt.Sprintf("Failed to connect redis.Conn, because of %v\n", err))
	} else {
		log.Printf("Succeeded to connect redis.Conn:addr=%v\n", server)
		return &cache{conn: conn}
	}
}

func (this *cache) withConn(fn func(redis.Conn) (interface{}, error)) (interface{}, error) {
	this.mu.Lock()
	defer this.mu.Unlock()
	return fn(this.conn)
}

func query(ctx groupcache.Context, key string, dst groupcache.Sink) error {
	if ctx == nil {
		return fmt.Errorf("nil Context is invalid")
	}

	// ex) [mtime]-[returntype]-[command]-[key]-[field] like 1417475105-str-HGET-ADINFO-1
	parts := strings.SplitN(key, "-", 5)
	if len(parts) < 4 {
		return fmt.Errorf("given key is invalid")
	}
	rettype := parts[1]
	command := parts[2]
	rkey := parts[3]
	var rfield string
	if len(parts) == 5 {
		rfield = parts[4]
	}

	c := ctx.(*cache)
	if got, err := c.withConn(func(conn redis.Conn) (interface{}, error) {
		return conn.Do(command, rkey, rfield)
	}); err != nil {
		log.Printf("Not Found origin data:err=%v, command=%v, rkey=%v, rfield=%v", err, command, rkey, rfield)
		return err
	} else {
		var ret string
		switch rettype {
		case "str":
			ret, err = redis.String(got, nil)
		case "int":
			var ret_int int
			if ret_int, err = redis.Int(got, nil); err == nil {
				ret = strconv.Itoa(ret_int)
			}
		default:
			return fmt.Errorf("Not Found rettype")
		}

		if err != nil {
			return err
		}

		dst.SetString(ret)
		return nil
	}
}

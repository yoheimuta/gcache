# README #

Redis をオリジンのデータストアにして、アプリサーバーで groupcache を動かすことを想定

```
# 1 つ目に立ち上げたサーバー (3000 で待ち受け)
# 処理に 935 us かかっている
great@Mac-158() gcache]$ GROUPCACHE_ADDR=127.0.0.1:8000 gcache
2014/12/04 08:17:42 Succeeded to connect redis.Conn:addr=localhost:6379
[martini] listening on :3000 (development)
[martini] Started GET /1417475105-str-HGET-ADGINFO-1 for 127.0.0.1:62630
[martini] Completed 200 OK in 935.408us
# 次に受けたリクエストは 112 us かかっている -> キャッシュされている
[martini] Started GET /1417475105-str-HGET-ADGINFO-1 for 127.0.0.1:62633
[martini] Completed 200 OK in 112.158us
```

```
# 2 つ目に立ち上げたサーバー (3001 で待ち受け)
# 処理に 361 us かかっている -> 3000 に問い合わせている
[great@Mac-158() gcache]$ GROUPCACHE_ADDR=127.0.0.1:8001 PORT=3001 gcache
2014/12/04 08:17:46 Succeeded to connect redis.Conn:addr=localhost:6379
[martini] listening on :3001 (development)
[martini] Started GET /1417475105-str-HGET-ADGINFO-1 for 127.0.0.1:62638
[martini] Completed 200 OK in 361.715us
# 次に受けたリクエストは 94 us かかっている -> ホットキャッシュされている
[martini] Started GET /1417475105-str-HGET-ADGINFO-1 for 127.0.0.1:62643
[martini] Completed 200 OK in 94.707us
```

### groupcache の特長

- ローカルにあればローカル、なければ他のピア、それもなければオリジン
- どこになにがあるべきかは、各ピアごとの URL Path（当然これは http request を待ち受ける url なりポートとは別に開ける） のハッシュ値をもとに、求める値のキーのハッシュ値が近いピアにて管理される
- 他のピアにあってもランダム（1/10 の確率）でローカルに持つ
- 限界まで溜まったら lru で消されていく
- 内部では protobuf しているけれど外側のインターフェースとしては関係がない
- 取得するためのメソッド実行を 1 回に制限するライブラリを自作して使っているので、そのメソッド実行中にそのメソッドを（他の goroutine が）後から呼ぼうとしてもブロックされる
